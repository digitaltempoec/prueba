<?php



namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PostalCodes;
use App\Models\PostalService;
use Exception;
use Illuminate\Http\Request;

class PostalServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $successStatus = 200;
    private $errorStatus = 500;
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        try {
            //$data= PostalService::with('federal_entity')->with('settlements')->with('municipality')->where('zip_code',$id)->get();
            $data = PostalCodes::where('d_codigo',$id)->get();

            foreach($data as $key => $value){
                $federal_entity[]=[
                    "name" => $value->d_ciudad,
                    "code" => $value->d_cp,
                ];

                $settlements[]=[
                    "name" => $value->d_asenta,
                    "zone_type" => $value->d_zona,
                    "settlement_type" =>$value->d_tipo_asenta,
                ];

                $municipality[]=[
                    "name" => $value->d_mnpio,
                ];

            }

            $dataCode=[
                'zip_code' =>$data[0]->d_codigo,
                'locality' =>$data[0]->d_estado,
                'federal_entity' => $federal_entity,
                'settlements' => $settlements,
                'municipality' =>$municipality,
            ];

            return response()->json(['status' => true, 'data' => $dataCode], $this->successStatus);
        } catch (Exception $e) {

            return response()->json(['status' => false, 'error' => 'Algo a sucedido por favor intente después de unos minutos', 'message' => $e->getMessage()], $this->errorStatus);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
